import org.apache.spark.sql.SparkSession

object Main {
  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().master("local[*]").appName("First spark app").getOrCreate()

    import spark.implicits._
    import org.apache.spark.sql.functions._

//    Task 1
    val posts = spark.read.json("bigdata20/posts_api.json")
    val foll_posts = spark.read.json("bigdata20/followers_posts_api_final.json")
    posts.show()
    foll_posts.show()
    //likes
    posts.sort($"likes".desc).limit(20).write.json("results/task1/posts_liked.json")
    foll_posts.sort($"likes").limit(20).write.json("results/task1/followers_posts_liked.json")

    //reposts
    posts.sort($"reposts".desc).limit(20).write.json("results/task1/posts_reposted.json")
    foll_posts.sort($"reposts").limit(20).write.json("results/task1/followers_posts_reposted.json")

    //comments
    posts.sort($"comments".desc).limit(20).write.json("results/task1/posts_commented.json")
    foll_posts.sort($"comments").limit(20).write.json("results/task1/followers_posts_commented.json")


    //Task 2
    val posts_likes = spark.read.parquet("bigdata20/posts_likes.parquet")
    posts_likes.show()
    posts_likes.groupBy("likerId").count().sort($"count".desc).limit(20)
      .write.json("results/task2/followers_posts_commented.json")

    foll_posts.printSchema()
    foll_posts.withColumn("copy_history", explode(col("copy_history")))
      .filter(col("copy_history")("owner_id") === "-94")
      .groupBy("owner_id").count().sort($"count".desc).limit(20).write
      .json("results/task2/followers_posts_commented.json")


    //Task 3
    foll_posts.withColumn("copy_history", explode(col("copy_history")))
      .filter(col("copy_history")("owner_id") === "-94")
      .select(col("copy_history")("id").alias("group_post_id"), col("id").alias("user_post_ids"))
      .sort("copy_history.id").groupBy("group_post_id").agg(collect_list("user_post_ids"))
      .sort("group_post_id").write.json("results/task3/followers_posts.json")


    //Task 4
    posts.createOrReplaceTempView("posts_table")

    spark.udf.register("get_emoji", (s: String) => """\p{block=Emoticons}""".r.findAllIn(s).toArray)
    spark.sql("SELECT key, get_emoji(text) as emoji FROM posts_table WHERE text is not null")
      .select("key", "emoji").coalesce(1).sort($"emoji".desc)
      .write.json("results/task4/posts_emojis.json")

    //Task 5

    val foll_posts_likes = spark.read.parquet("bigdata20/followers_posts_likes.parquet")
    val foll_posts_likes_selected = foll_posts_likes.select("ownerId", "likerId")
      .where(col("ownerId") =!= col("likerId")).distinct()

    foll_posts_likes_selected.alias("x").join(foll_posts_likes_selected.alias("y"),
      col("x.ownerId") === col("y.likerId")
        && col("x.likerId") === col("y.ownerId"))
      .select($"x.ownerId".alias("user_id"), $"y.ownerId".alias("friend_id"))
      .sort("friend_id")
      .groupBy("user_id")
      .agg(collect_list("friend_id"))
      .write.json("results/task5/friends.json")


    //Task 6
    foll_posts_likes_selected.alias("x").join(foll_posts_likes_selected.alias("y"),
          col("x.ownerId") === col("y.likerId")
            && col("x.likerId") === col("y.ownerId"), "left_outer")
          .where($"y.likerId".isNull)
          .select($"x.ownerId".alias("user_id"), $"x.likerId".alias("fan_id"))
          .sort("fan_id")
          .groupBy("user_id")
          .agg(collect_list("fan_id"))
          .write.json("results/task6/fans.json")


  }
}
